﻿using OTUS_Prototype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_Prototype.Services
{
    public class AdminUserService
    {
        public static void Test()
        {
            AdminUser adminUser1 = new AdminUser(5, "TestAdminUser", "1234567", new List<string>() { "Page1", "Page2" }, false);

            AdminUser adminUser2 = adminUser1.MyClone();
            adminUser2.Id = 6;
            adminUser2.Password = "password";
            adminUser2.Pages.Add("Page3");
            adminUser2.IsSuperAdmin = true;

            AdminUser adminUser3 = (AdminUser)adminUser1.Clone();
            adminUser3.Id = 7;
            adminUser3.Login = "CloneAdminUser";
            adminUser3.Pages.Add("Page3");
            adminUser3.Pages.Add("Page4");

            Console.WriteLine($"Original: {adminUser1}");
            Console.WriteLine($"MyClone: {adminUser2}");
            Console.WriteLine($"Clone: {adminUser3}");
        }
    }
}
