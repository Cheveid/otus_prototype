﻿using OTUS_Prototype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_Prototype.Services
{
    public class UserService
    {
        public static void Test()
        {
            User user1 = new User(1, "TestUser", "123");

            User user2 = user1.MyClone();
            user2.Id = 2;
            user2.Login = "AnotherUser";

            User user3 = (User)user1.Clone();
            user3.Id = 3;
            user3.Login = "CloneUser";

            Console.WriteLine($"Original: {user1}");
            Console.WriteLine($"MyClone: {user2}");
            Console.WriteLine($"Clone: {user3}");
        }
    }
}
