﻿using OTUS_Prototype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_Prototype.Services
{
    public class ExtendUserService
    {
        public static void Test()
        {
            ExtendUser extendUser1 = new ExtendUser(3, "TestExtendUser", "12345", new List<string>() { "Page1", "Page2" });

            ExtendUser extendUser2 = extendUser1.MyClone();
            extendUser2.Id = 4;
            extendUser2.Login = "AnotherExtendUser";
            extendUser2.Pages.RemoveAt(extendUser2.Pages.Count - 1);
            extendUser2.Pages.Add("Page3");

            ExtendUser extendUser3 = (ExtendUser)extendUser1.Clone();
            extendUser3.Id = 5;
            extendUser3.Login = "CloneExtendUser";
            extendUser3.Pages.Add("Page3");

            Console.WriteLine($"Original: {extendUser1}");
            Console.WriteLine($"MyClone: {extendUser2}");
            Console.WriteLine($"Clone: {extendUser3}");
        }
    }
}
