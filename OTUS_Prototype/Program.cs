﻿using OTUS_Prototype.Services;
using System;

namespace OTUS_Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            UserService.Test();

            Console.WriteLine();

            ExtendUserService.Test();

            Console.WriteLine();

            AdminUserService.Test();

            Console.ReadKey();
        }
    }
}
