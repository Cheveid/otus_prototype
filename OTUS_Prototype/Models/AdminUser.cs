﻿using OTUS_Prototype.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_Prototype.Models
{
    /// <summary>
    /// Администратор
    /// </summary>
    public class AdminUser : ExtendUser, IMyCloneable<AdminUser>, ICloneable
    {
        /// <summary>
        /// Признак супер-администратора
        /// </summary>
        public bool IsSuperAdmin { get; set; }

        public AdminUser(int id, string login, string password, List<string> pages, bool isSuperAdmin)
            : base(id, login, password, pages)
        {
            IsSuperAdmin = isSuperAdmin;
        }

        public AdminUser(AdminUser adminUser) : base(adminUser)
        {
            IsSuperAdmin = adminUser.IsSuperAdmin;
        }

        /// <summary>
        /// Клонирование через интерфейс IMyCloneable
        /// </summary>
        /// <returns>Новый объект AdminUser</returns>
        public override AdminUser MyClone()
        {
            return new AdminUser(this);
        }

        /// <summary>
        /// Клонирование через интерфейс ICloneable
        /// </summary>
        /// <returns>Новый объект</returns>
        public override object Clone()
        {
            return MyClone();
        }

        public override string ToString()
        {
            return $"{base.ToString()}, IsSuperAdmin={IsSuperAdmin}";
        }
    }
}
