﻿using OTUS_Prototype.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_Prototype.Models
{
    /// <summary>
    /// Расширенная версия Пользователя
    /// </summary>
    public class ExtendUser: User, IMyCloneable<ExtendUser>, ICloneable
    {
        /// <summary>
        /// Список доступных страниц
        /// </summary>
        public List<string> Pages { get; set; }

        public ExtendUser(int id, string login, string password, List<string> pages)
            : base(id, login, password)
        {
            Pages = pages;
        }

        public ExtendUser(ExtendUser extendUser)
            : base(extendUser)
        {
            Pages = new List<string>();
            extendUser.Pages.ForEach(s => Pages.Add(s));
        }

        /// <summary>
        /// Клонирование через интерфейс IMyCloneable
        /// </summary>
        /// <returns>Новый объект ExtendUser</returns>
        public override ExtendUser MyClone()
        {
            return new ExtendUser(this);
        }

        /// <summary>
        /// Клонирование через интерфейс ICloneable
        /// </summary>
        /// <returns>Новый объект</returns>
        public override object Clone()
        {
            return MyClone();
        }

        public override string ToString()
        {
            return $"{base.ToString()}, Pages=[{String.Join(", ", Pages.ToArray())}]";
        }
    }
}
