﻿using OTUS_Prototype.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_Prototype.Models
{
    /// <summary>
    /// Базовый класс "Пользователь"
    /// </summary>
    public class User: IMyCloneable<User>, ICloneable
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }

        public User(int id, string login, string password)
        {
            Id = id;
            Login = login;
            Password = password;
        }

        public User(User user)
        {
            Id = user.Id;
            Login = user.Login;
            Password = user.Password;
        }

        /// <summary>
        /// Клонирование через интерфейс IMyCloneable
        /// </summary>
        /// <returns>Новый объект User</returns>
        public virtual User MyClone()
        {
            return new User(this);
        }

        /// <summary>
        /// Клонирование через интерфейс ICloneable
        /// </summary>
        /// <returns>Новый объект</returns>
        public virtual object Clone()
        {
            return MyClone();
        }

        public override string ToString()
        {
            return $"Id={Id}, Login={Login}, Password={Password}";
        }
    }
}
